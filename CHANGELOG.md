# Changelog

All notable changes to this project will be documented in this file.

OtpClient project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] 

### Added

- Initial release with basic functionality

### Changed

- Nothing yet
