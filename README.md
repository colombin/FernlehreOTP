﻿# OtpClient
Made by Dorner Ingemar, Schmidl Markus, Witt Markus  
Made for the course "Mobile and Embedded Security UE"  

## Requirements

To build this software, you need the .NET Core 3.1 SDK installed.

The software has only been tested on Ubuntu 20.04.  

The Programm only works with the Token "Feitian ePass2003"!

## Building

To build the solution, simply run:

    dotnet build
    
To create a standalone binary, run:

    dotnet publish -r linux-x64 -p:PublishSingleFile=true --self-contained
   
This will create a binary file `OtpClient` in the folder:  
`OtpClient/bin/Debug/netcoreapp3.1/linux-x64/publish/`.
   
## Install

After creating the standalone binary, simply copy it to a path within your PATH environment

## Help message. Flag description.

 -h                 (Default: false) Show Help Text

  -n, --new          (Default: false) Create a new account, you have to add the
                     name the secret and the password

  -o, --otp          (Default: false) Get OneTimePassword for Named Account, you
                     have to add password

  -l, --list         (Default: false) Show all objects, you have to add password

  -d, --delete       (Default: false) Deletes the named Account, you have to add
                     password

  -N, --name         Name for the account

  -p, --pass         String for password

  -s, --secret       Secret for the new account (BASE32 encoded)

  -k, --keylenght    (Default: 6) Lenght of the OneTimePassword(6-8), default is
                     6 digits

  -t, --time         (Default: 30) Token periode in seconds, default is 30

  -m, --hashmode     (Default: 0) Choose Haschmode, 0 for sha1, 1 for sha256, 2
                     for

## Usage
### Create account

    OtpClient -n -N "MyGoogleAccountName" -s SECRET234 -p "1234"

    Optional parameters:

    OtpClient -n -N "MyGoogleAccountName" -s SECRET234 -p "1234" -t 60 -m 2 -k 8

### Get OTP password

    OtpClient -o -N "MyGoogleAccountName" -p "1234"

### List all accounts

    OtpClient -l -p "1234"

### Delete account

    OtpClient -d -N "MyGoogleAccountName" -p "1234"

The delete function only works if you reconfigure and initialize the token. You have to change the configuration file `/usr/share/opensc/epass2003.profile`.

data objects are stored in transparent EFs:
```
EF privdata {
    file-id = 3400;
    structure = transparent;
    ACL = *=NEVER,READ=$PIN,UPDATE=$PIN,DELETE=$PIN;
}  
```
You have to add `DELETE=$PIN` like in the upper box.

Then you have to delete the token and initialize it:

    pkcs15-init -E

    pkcs15-init -C -p pkcs15+onepin --pin 1234 --puk 123456


## Limitations

You have to give the password/pin every time you use the program. Deletation is just possible with changing the configuration of the Epass2003 Token. We couldn't figure out how to do it without that.