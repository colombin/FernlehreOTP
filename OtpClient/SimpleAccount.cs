﻿using System;
using OtpNet;

namespace OtpClient
{
    /// <summary>
    /// A simple in-memory implementation of an OTP account
    /// </summary>
    public class SimpleAccount : IAccount
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:OtpClient.SimpleAccount"/> class.
        /// </summary>
        /// <param name="name">Name of the key</param>
        /// <param name="secret">BASE32 encoded secret for the OTP key</param>
        public SimpleAccount(string name, string secret, int lenght, int time, OtpHashMode hash)
        {
            Name = name;
            Secret = secret;
            Parameters = new TotpParameters();
            Parameters.Length = lenght;
            Parameters.Step = time;
            Parameters.HashMode = hash;
        }


        public string Name { get; private set; }

        /// <summary>
        /// The BASE32 encoded secret for the OTP calculation
        /// </summary>
        public string Secret { get; set; }

        public IKeyProvider Key
        {
            get
            {
                return new InMemoryKey(Base32.ToByteArray(Secret.ToUpper()));
            }
        }

        public TotpParameters Parameters { get; set; }
    }
}
