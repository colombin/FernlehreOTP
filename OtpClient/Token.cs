using System;
using System.Collections.Generic;
using Net.Pkcs11Interop.Common;
using Net.Pkcs11Interop.HighLevelAPI;
using Net.Pkcs11Interop.LowLevelAPI80;
using ServiceStack.Text;
using OtpNet;

namespace OtpClient
{

    class Token
    {
        public ISession session;
        public int Initialize(string password)
        {
            // string pkcs11LibraryPath = "/usr/lib/softhsm/libsofthsm2.so";
            string pkcs11LibraryPath = "/usr/lib/x86_64-linux-gnu/opensc-pkcs11.so";
            // Create factories used by Pkcs11Interop library
            Pkcs11InteropFactories factories = new Pkcs11InteropFactories();
            IPkcs11Library pkcs11Library;
            try
            {
                // Load unmanaged PKCS#11 library
                pkcs11Library = factories.Pkcs11LibraryFactory.LoadPkcs11Library(factories, pkcs11LibraryPath, AppType.MultiThreaded);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e.Message);
                return 1;
            }

            List<ISlot> slotList = pkcs11Library.GetSlotList(SlotsType.WithTokenPresent);
            if (slotList.Count == 0)
            {
                Console.WriteLine("Keinen verfügbaren Slot gefunden.");
                return 1;
            }
            else
            {
                foreach (ISlot slot in slotList)
                {
                    ISlotInfo slotInfo = slot.GetSlotInfo();
                    // if (String.Equals("SoftHSM slot ID 0x574116c", slotInfo.SlotDescription))
                    if (String.Equals("Feitian ePass2003 00 00", slotInfo.SlotDescription))
                    {
                        session = slot.OpenSession(SessionType.ReadWrite);
                        session.Login(CKU.CKU_USER, password);
                    }
                }
            }
            return 0;

        }
        public bool Create(string account, string secret)
        {
            List<IObjectAttribute> findAttribute = new List<IObjectAttribute>();
            List<IObjectHandle> findHandle = new List<IObjectHandle>();
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_CLASS, CKO.CKO_DATA));
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_LABEL, account));
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_TOKEN, true));
            session.FindObjectsInit(findAttribute);
            findHandle = session.FindObjects(1);
            session.FindObjectsFinal();
            if (findHandle.Count > 0)
            {
                Console.WriteLine("Account already exists on Token. Use different account name or delete it.");
                return false;
            }

            List<IObjectAttribute> objectAttributes = new List<IObjectAttribute>();
            objectAttributes.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_CLASS, CKO.CKO_DATA));
            objectAttributes.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_TOKEN, true));
            objectAttributes.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_LABEL, account));
            objectAttributes.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_VALUE, secret));
            objectAttributes.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_PRIVATE, true));

            IObjectHandle objectHandle = session.CreateObject(objectAttributes);
            return true;
        }

        public void ObjList()
        {
            List<IObjectAttribute> findAttribute = new List<IObjectAttribute>();
            List<IObjectHandle> findHandle = new List<IObjectHandle>();
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_CLASS, CKO.CKO_DATA));
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_TOKEN, true));
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_PRIVATE, true));
            findHandle = session.FindAllObjects(findAttribute);

            List<CKA> ckaAttributes = new List<CKA>();
            ckaAttributes.Add(CKA.CKA_LABEL);
            ckaAttributes.Add(CKA.CKA_VALUE);
            foreach (var item in findHandle)
            {
                List<IObjectAttribute> attList = new List<IObjectAttribute>();
                attList = session.GetAttributeValue(item, ckaAttributes);
                Console.WriteLine("Account: " + attList[0].GetValueAsString());
            }
        }

        public bool ObjRead(string label)
        {
            List<IObjectAttribute> findAttribute = new List<IObjectAttribute>();
            List<IObjectHandle> findHandle = new List<IObjectHandle>();
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_CLASS, CKO.CKO_DATA));
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_LABEL, label));
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_TOKEN, true));
            findAttribute.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_PRIVATE, true));
            session.FindObjectsInit(findAttribute);
            findHandle = session.FindObjects(1);
            session.FindObjectsFinal();

            List<CKA> ckaAttributes = new List<CKA>();
            ckaAttributes.Add(CKA.CKA_LABEL);
            ckaAttributes.Add(CKA.CKA_VALUE);
            if (findHandle.Count == 0)
            {
                Console.WriteLine("Account doesn't exist.");
                return false;
            }
            foreach (var item in findHandle)
            {
                List<IObjectAttribute> attList = new List<IObjectAttribute>();
                attList = session.GetAttributeValue(item, ckaAttributes);

                IAccount account = JsonSerializer.DeserializeFromString<IAccount>(attList[1].GetValueAsString());

                var totp = new Totp(account.Key, account.Parameters.Step, account.Parameters.HashMode, account.Parameters.Length);
                string otp = totp.ComputeTotp();

                Console.WriteLine(account.Name + ": " + otp);
                break;
            }
            return true;
        }
        public bool ObjDestroy(string account)
        {
            List<IObjectAttribute> objectAttributes = new List<IObjectAttribute>();
            objectAttributes.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_CLASS, CKO.CKO_DATA));
            objectAttributes.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_TOKEN, true));
            objectAttributes.Add(session.Factories.ObjectAttributeFactory.Create(CKA.CKA_LABEL, account));
            session.FindObjectsInit(objectAttributes);
            List<IObjectHandle> foundObjects = session.FindObjects(1);
            if (foundObjects.Count == 0)
            {
                Console.WriteLine("Cannot delete account, account doesn't exist");
                return false;
            }

            session.FindObjectsFinal();
            IObjectHandle accountToDelete = foundObjects[0];
            ISessionInfo sessionInfo2 = session.GetSessionInfo();
            session.DestroyObject(accountToDelete);
            return true;
        }

        public string StoreAccounts(IAccount test)
        {
            string serialized = JsonSerializer.SerializeToString(test);
            return serialized;

        }
    }
}