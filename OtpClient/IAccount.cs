﻿using System;
using OtpNet;

namespace OtpClient
{
    /// <summary>
    /// Interface for a OTP account
    /// </summary>
    public interface IAccount
    {
        /// <summary>
        /// Descriptive name of the account
        /// </summary>
        String Name { get; }
        /// <summary>
        /// A key provider which will hold the secret and calculate the hashes
        /// </summary>
        IKeyProvider Key { get; }
        /// <summary>
        /// Parameters for the OTP calculation (like the length of the OTP)-
        /// </summary>
        TotpParameters Parameters { get; set;  }
    }
}
