﻿using System;
using OtpNet;

namespace OtpClient
{
    public class TotpParameters
    {
        public TotpParameters()
        {
        }

        private int step = 30;
        public int Step
        {
            get { return step; }
            set
            {
                if (value <= 0)
                    throw new ArgumentException("Step must be positive");
                step = value;
            }
        }
        private int length = 6;

        public int Length
        {
            get { return length; }
            set
            {
                if (value < 6 || value > 8)
                    throw new ArgumentException("Length must be 6, 7 or 8.");
                length = value;
            }
        }

        private OtpHashMode hashMode = OtpHashMode.Sha1;

        public OtpHashMode HashMode
        {
            get { return hashMode; }
            set
            {
                hashMode = value;
            }
        }
    }
}
