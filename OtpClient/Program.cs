﻿using System;
using System.IO;
using OtpNet;
using CommandLine;
using CommandLine.Text;

namespace OtpClient
{
    class CliOptions
    {
        [Option('h', Default = false, HelpText = "Show Help Text")]
        public bool HelpText { get; set; }

        [Option('n', "new", Default = false, HelpText = "Create a new account, you have to add the name the secret and the password")]
        public bool createAccount { get; set; }

        [Option('o', "otp", Default = false, HelpText = "Get OneTimePassword for Named Account, you have to add password")]
        public bool getOTP { get; set; }

        [Option('l', "list", Default = false, HelpText = "Show all objects, you have to add password")]
        public bool ObjList { get; set; }

        [Option('d', "delete", Default = false, HelpText = "Deletes the named Account, you have to add password")]
        public bool deleteAccount { get; set; }

        [Option('N', "name", Default = null, HelpText = "Name for the account")]
        public string AccountName { get; set; }

        [Option('p', "pass", Default = null, HelpText = "String for password")]
        public string PassString { get; set; }

        [Option('s', "secret", Default = null, HelpText = "Secret for the new account (BASE32 encoded)")]
        public string NewAccountSecret { get; set; }

        [Option('k', "keylenght", Default = 6, HelpText = "Lenght of the OneTimePassword(6-8), default is 6 digits")]
        public int keylenght { get; set; }

        [Option('t', "time", Default = 30, HelpText = "Token periode in seconds, default is 30")]
        public int timePeriode { get; set; }

        [Option('m', "hashmode", Default = 0, HelpText = "Choose Hachmode, 0 for sha1, 1 for sha256, 2 for sha512")]
        public int hashmode { get; set; }
    }

    class Program
    {

        static void Main(string[] args)
        {
            bool showHelp = false;
            CliOptions options = new CliOptions();

            var parser = new CommandLine.Parser(with => with.HelpWriter = null);
            var argsParseResult = parser.ParseArguments<CliOptions>(args);
            argsParseResult.WithParsed<CliOptions>((opts) => options = opts)
                .WithNotParsed<CliOptions>((errs) =>
                {
                    foreach (Error err in errs)
                    {
                        if (!((err is CommandLine.HelpRequestedError) || (err is CommandLine.VersionRequestedError)))
                        {
                            Console.Error.WriteLine("ERROR: Commandline parsing error.");
                            Console.WriteLine(err);
                        }
                        showHelp = true;
                    }
                });
            if (options.HelpText)
            {
                showHelp = true;
            }
            else if (options.PassString == null || options.PassString == "")
            {
                Console.WriteLine("You have to give a password.");
                showHelp = true;
            }
            else
            {
                var token = new Token();
                if (!showHelp)
                {
                    int retinit = token.Initialize(options.PassString);
                    if (retinit == 1)
                    {
                        return;
                    }
                }

                if (options.createAccount && !options.getOTP && !options.deleteAccount && !options.ObjList)
                {
                    if (options.AccountName == null || options.AccountName == "")
                    {
                        Console.Error.WriteLine("ERROR: No name for the new account specified.");
                        showHelp = true;
                    }
                    if (options.NewAccountSecret == null || options.NewAccountSecret == "")
                    {
                        Console.Error.WriteLine("ERROR: No secret for the new account specified.");
                        showHelp = true;
                    }
                    try
                    {
                        OtpHashMode hash;
                        switch (options.hashmode)
                        {
                            case 0:
                                hash = OtpHashMode.Sha1;
                                break;
                            case 1:
                                hash = OtpHashMode.Sha256;
                                break;
                            case 2:
                                hash = OtpHashMode.Sha512;
                                break;
                            default:
                                throw new ArgumentException("Hashmode not supported!");
                        }
                        var newAccount = new SimpleAccount(options.AccountName, options.NewAccountSecret, options.keylenght, options.timePeriode, hash);

                        if (!(token.Create(options.AccountName, token.StoreAccounts(newAccount))))
                        {
                            return;
                        }

                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine("ERROR: Could not create the new account: {0}", e.Message);
                    }
                }
                else if (options.deleteAccount && !options.createAccount && !options.getOTP && !options.ObjList)
                {
                    if (!(token.ObjDestroy(options.AccountName)))
                    {
                        return;
                    }
                }
                else if (options.getOTP && !options.createAccount && !options.deleteAccount && !options.ObjList)
                {
                    if (!(String.IsNullOrEmpty(options.AccountName)))
                    {
                        if (!(token.ObjRead(options.AccountName)))
                        {
                            return;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error: No Account name available.");
                        showHelp = true;
                    }
                }
                else if (options.ObjList && !options.createAccount && !options.deleteAccount && !options.getOTP)
                {
                    token.ObjList();
                }
                else
                {
                    if (options.HelpText)
                    {
                        Console.WriteLine("Error, no main function n, d, l or o selected!");
                        showHelp = true;
                    }
                }
            }

            if (showHelp)
            {
                var test = HelpText.AutoBuild(argsParseResult, h => h, r => r);
                Console.WriteLine(test);

                return;
            }
        }
    }
}

